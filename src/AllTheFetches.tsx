
/*
export const setSaveTime = (id: string, millSec: number, sec: number, min: number) =>
    fetch("http://localhost:8080/api/time/saveTime",{
        method: "POST",
        headers: {
            id: id,
            "Content-Type": "application/json"
        }
        ,
        body: {
            milliSec: millSec,
            sec: sec,
            minute: min
        }

    })*/

export const setSaveTime = async (millSec: number, sec: number, min: number) => {
    const response = await fetch("http://localhost:8080/api/time/saveTime", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "mill": millSec.toString(),
            "sec": sec.toString(),
            "min": min.toString(),
        },

    });

    if (!response.ok) {
        const error = await response.json();
        throw new Error(error.message || "Unknown error occurred");
    }

    return await response.json();
};


export const getAllSavedTimes = () =>
    fetch("http://localhost:8080/api/time", {
    })
        .then(response => response.json())

