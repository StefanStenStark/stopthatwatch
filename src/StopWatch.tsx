import React, {useEffect, useState} from "react";
import {getAllSavedTimes, setSaveTime} from "./AllTheFetches";

function Stopwatch(){
    const [secoundTimer, setSecTime] = useState(0);
    const [miliSecoundTimer, setMilTime] = useState(0);
    const [minuteTimer, setMinTime] = useState(0);
    const [isRunning, setIsRunning] = useState(false);
    const [showTimeList, setShowTimeList] = useState(false)
    const [SavedTimes, setSavedTimes] = useState([])


    const doNotShotListThankYou = () =>{
        setShowTimeList(false);
    }
    const showListThankYou = () =>{
        setShowTimeList(true);
        getSavedTimes();
    }
    const getSavedTimes = () => {
        getAllSavedTimes()
            .then((allTheTimes: React.SetStateAction<never[]>) => {
                setSavedTimes(allTheTimes)
            })
        //.then(data => console.log(data))
    }

    useEffect(() => {
        let miliSeconds: number | undefined;
        if (isRunning) {
            miliSeconds = setInterval(() => setMilTime(miliSecoundTimer + 1), 10);
            if (miliSecoundTimer >= 100 ){
                setMilTime(0)
            }
        }

        return () => clearInterval(miliSeconds);
    }, [isRunning, miliSecoundTimer]);


    useEffect(() => {
        let seconds: number | undefined;
        if (isRunning) {
            seconds = setInterval(() => setSecTime(secoundTimer + 1), 1000);
            if (secoundTimer >= 60){
                setSecTime(0)
            }

        }
        return () => clearInterval(seconds);
    }, [isRunning, secoundTimer]);

    useEffect(() => {
        let minutes: number | undefined;
        if (isRunning) {
            minutes = setInterval(() => setMinTime(minuteTimer + 1), 60000);
        }
        return () => clearInterval(minutes);
    }, [isRunning, minuteTimer]);


    function startTimer(){
        setIsRunning(true)
    }

    function stopTimer(){
        setIsRunning(false)
    }
    function resetTimer(){
        setIsRunning(false)
        setMilTime(0)
        setSecTime(0)
        setMinTime(0)
    }
    return(
        <>
            <h1>Min: {minuteTimer}, Sec: {secoundTimer}, DeciS: {miliSecoundTimer}</h1>

            <button onClick={() => {
                startTimer();
            }}>
                Start
            </button>

            <button onClick={() => {
                stopTimer();
            }}>
               Stop
            </button>

            <button onClick={() => {
                resetTimer();
            }}>
                Reset
            </button>
            <button onClick={() => {
                setSaveTime(miliSecoundTimer, secoundTimer, minuteTimer);
            }}>
                Save plz
            </button>
            {
                showTimeList?<div>

                </div>:<button onClick={() => {
                    showListThankYou();
                }}>
                    Show list of time
                </button>
            }
            {
                showTimeList?<div>
                    <button onClick={() => {
                        doNotShotListThankYou();
                    }}>
                        Hide List of time
                    </button>
                </div>:null
            }

            {
                showTimeList?<div>
                    <ul>

                        {SavedTimes.map((times: {milliseconds: string, seconds: string, minutes: string}) => (
                            <AllTheTimes
                                milliseconds={times.milliseconds}
                                seconds={times.seconds}
                                minutes={times.minutes}
                            ></AllTheTimes>
                        ))
                        }
                    </ul>
                </div>:null
            }


        </>


    )
}

interface ITimes {
    milliseconds: string
    seconds: string
    minutes: string
}

function AllTheTimes({milliseconds, seconds, minutes}: ITimes) {
    return(
        <li>
            {minutes} Minutes {seconds} Seconds  {milliseconds} Decisecond
        </li>
    )
}

export default Stopwatch